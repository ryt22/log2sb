
var azure = require('azure');
var moment = require('moment');
var Chance = require('chance');
///
var chance = new Chance();

///
// mandatory--> connString, busName, queueOpt
////
function serviceBus(sbusOpt){
    this.sbusConn = azure.createServiceBusService(sbusOpt.connString);
    this.sbusName = sbusOpt.busName;
    this.squeueOpt = sbusOpt.queueOpt;
    this.entityName = sbusOpt.entityName || chance.name().replace(" ","-");
    this.busOK = false;
}
////
serviceBus.prototype.push = function(metaMsg,contentMsg){
    var self = this;
    ///
    var submitMsg = {
        metadata:{
            entity: self.entityName,
            timestamp: moment().format('YYYY-MM-DD HH:mm:ss')
        },
        content: contentMsg
    };
    /// combine metadata
    submitMsg.metadata = Object.assign(submitMsg.metadata,metaMsg);
    //// serialize
    submitMsg = JSON.stringify(submitMsg);
    /////
    return new Promise(function(resolve,reject){
        self.sbusConn.createQueueIfNotExists(self.sbusName, self.squeueOpt, function(error){
            if(!error){
                self.sbusConn.sendQueueMessage(self.sbusName, submitMsg, function(error){
                    if(!error) resolve('OK');
                    else reject('FAIL_SEND');
                });
            }
            else reject('FAIL_SBUS');
        });
    });
};
////
module.exports = serviceBus;
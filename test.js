var testdata = require('./testdata.json');
var cstring = testdata.cstring;
/////
var log2sb = require("./index");
///
var qLog = new log2sb({
    connString: cstring,
    busName: "log-queue"
});
///
var meta_test = {
    flowID: "ABC123",
    from: "source",
    to: "target"
}
///
var data_test = {
    body: 'Test message 20906',
    customProperties: {
        testproperty: 'TestValue 2211'
    }
};
///
qLog.push(meta_test,data_test)
.then(function(status){
    console.log(status);
})
.catch(function(e){
    console.log(e);
});
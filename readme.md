# log2sb
Push String or Object to Azure service bus queue. 
Mainly used as an alternative of pushing critical log entries to Azure Service Bus Queue. 

---

### Usage
```javascript
var log2sb = require(log2sb);

//// define the config
var config = {
    connString: "your-connection-string",
    busName: "your-queue-name",
    entityName: "your-entity-or-service" // should be given, otherwise random name will be generated on start
    // queueOpt --> optional, please follow Azure ServiceBus queue documentation on queue create
};

/// create log service
var logService = new log2sb(config);

// String data
var myData = "This is sample";
// Object data
var myData = {
    firstData: "This is sample",
    nextData: "Another sample"
};
// Metadata
var myMeta = {
    flowID: "ABC123",
    from: "source",
    to: "target"
};

/// push log service
logService.push(myMeta,myData)
.then(function(status){
    console.log(status);
})
.catch(function(e){
    console.log(e);
});
```

### Returned Promise
| Type          | Value         |
| ------------- |:-------------:|
| Resolve       | OK            |
| Resolve       | FAIL_SEND     |
| Reject        | FAIL_SBUS     |